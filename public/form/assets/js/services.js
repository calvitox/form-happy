(function () {
    angular.module('happyForm.services', [])
        .factory("apiService", function ($http, $q, API_URL) {
            var api = {};

            api.get = function (url) {
                var deferred = $q.defer();
                $http({
                    url: API_URL + url,
                    method: 'GET',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                }).then(function successCallback(response) {
                    deferred.resolve(response);
                }, function errorCallback(response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            };

            api.post = function (url, datos) {
                var deferred = $q.defer();
                $http({
                    url: API_URL + url,
                    data: datos,
                    method: 'POST',
                }).then(function successCallback(response) {
                    deferred.resolve(response);
                }, function errorCallback(response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            };

            return {
                api: api
            };
        })
})();

