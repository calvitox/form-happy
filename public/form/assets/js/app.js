'use strict';

// Declare app level module which depends on views, and components
angular.module('happyForm', [
    'ui.router',
    'happyForm.constants',
    'happyForm.services',
    'ui.bootstrap',
]).config(function ($locationProvider, $urlRouterProvider, $logProvider) {
    $locationProvider.hashPrefix('!');
    $urlRouterProvider.otherwise('/');
    //$logProvider.debugEnabled(false);
});
