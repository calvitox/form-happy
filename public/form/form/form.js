'use strict';

angular.module('happyForm')
    .config(function ($stateProvider) {
        $stateProvider.state({
            name: 'formulario',
            url: '/',
            controller: 'MainCtrl',
            templateUrl: 'form/form.html'
        });
    })

    .controller('MainCtrl', ['$scope','$stateParams', '$location' ,'apiService', function ($scope, $stateParams, $location , apiService){
    	$scope.success = false;
        $scope.cliente = {
    		id_sede: $location.search().id_sede
    	};

    	apiService.api.get('/sedes').then(function (res){
    		$scope.sedes = res.data;
    	});

    	$scope.guardar = function (form){
    		var cliente = $scope.cliente;
    		if (form.$invalid) {
    			return;
    		};

    		apiService.api.post('/cliente', cliente).then(function (res){
                $scope.cliente = {};
                $scope.success = true;
    			console.log(form);
    		});
    	}
    }])