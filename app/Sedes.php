<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sedes extends Model
{
    public function usuarios(){
    	$this->hasMany('App\Clientes', 'id_sede');
    }
}
