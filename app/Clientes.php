<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
	protected $fillable = ['nombres','cedula','apellidos','correo','foto','numero_contacto','id_sede'];

    public function sede(){
    	$this->belongsTo('App\Sedes', 'id_sede');
    }
}
